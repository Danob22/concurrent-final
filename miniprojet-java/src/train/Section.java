package train;

/**
 * Représentation d'une section de voie ferrée. C'est une sous-classe de la
 * classe {@link Element}.
 *
 * @author Fabien Dagnat <fabien.dagnat@imt-atlantique.fr>
 * @author Philippe Tanguy <philippe.tanguy@imt-atlantique.fr>
 * Modifié par  : Arias et Ducuara
 */
public class Section extends Element {	
	private boolean occupied; //Variable pour assurer l'invariant
	
	public Section(String name) {
		super(name);
		this.occupied = false;
	}
	
	/**
	 * Condition d'attente
	 * @return True : Le train peut entrer à la section
	 */
	@Override
	public boolean getAccess() {
		return !occupied;
	}
	
	/**
	 * Il n'y a pas de condition de sortie, 
	 * dans une section, le train pourra toujours sortir
	 * @return True : le train peut sortir de la section
	 */
	@Override
	public boolean getLeave(Direction d) {
		return true;
	}
    
	/**
     * Un train entre dans la section, celui-ci est occupé.
     * Section critique 
	 */
	@Override	
	public synchronized void enter(Direction d) {
		this.occupied = true;
		
		
	}

	/**
	 * Un train sort de la section, celui-ci est vide. 
	 * Section critique 
	 */
	@Override
	public synchronized void leave(Direction d)  {
		this.occupied = false;
		
	
	}

	
	
	
	
	
}
