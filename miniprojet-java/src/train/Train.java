package train;

/**
 * Représentation d'un train. Un train est caractérisé par deux valeurs :
 * <ol>
 *   <li>
 *     Son nom pour l'affichage.
 *   </li>
 *   <li>
 *     La position qu'il occupe dans le circuit (un élément avec une direction) : classe {@link Position}.
 *   </li>
 * </ol>
 *
 * Il a besoin de connaitre son contrôleur pour déclancher l'action
 * @author Fabien Dagnat <fabien.dagnat@imt-atlantique.fr>
 * @author Mayte segarra <mt.segarra@imt-atlantique.fr>
 * Test if the first element of a train is a station
 * @author Philippe Tanguy <philippe.tanguy@imt-atlantique.fr>
 * @version 0.3
 * Modifié par  : Arias et Ducuara
 * 
 * Le train est un objet actif, il implement Runnable
 * Nouvelle action :  enter.
 * 
 */
public class Train implements Runnable {
	private final String name;
	private final Railway r; //ressource partage
	private Position pos;    //Change final, it must change position
	

	public Train(String name, Position p,Railway r) throws BadPositionForTrainException,NotPlaceGareException {
		if (name == null || p == null)
			throw new NullPointerException();

		//Un train doit d'abord être dans une gare 
		if (!(p.getElement() instanceof Station))
			throw new BadPositionForTrainException(name);
		
		//Contrôle des erreurs dans l'initialisation du système
		if (!(p.getElement().getAccess()))
			throw new NotPlaceGareException(name); //nouvelle exception
		
		//Train en gare pour la première fois
		p.getElement().enter(p.getDirection()); 
		this.name = name;
		this.pos = p.clone();
		this.r = r;
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder("Train[");
		result.append(this.name);
		result.append("] ----> ");
		result.append(this.pos);
		return result.toString();
	}
	/**
	 * Méthode pour tirer l'action du chemin de fer
	 */
	private void enter() {
	   this.pos = this.r.enter(this.pos);   //Appel à methode synchro	
	   //L'affichage d'état
	   System.out.println(this+
			   " \nSur la voie : LR("+r.road.getLR()+") - RL("+r.road.getRL()+") ");
	}

	/*
	 * 
	 * (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		while(true) {		
			try {			
				this.enter();
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}			
		}		
	}
	
	
}
