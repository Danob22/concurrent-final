package train;

/**
 * Classe responsable du contrôle des trains sur la voie
 * @author Arias et Ducuara
 * 
 *
 */
public class Road {
    private int trainLR; // Variable pour assurer l'invariant
    private int trainRL; // Variable pour assurer l'invariant
	
    public Road(String name) {
    	this.trainLR = 0;
    	this.trainRL = 0;
    }
    
    /**
     * Condition d'attente
	 * @return True : Le train peut entrer dans le sens RL
     */
	public boolean getAccessRL() {
    	return (trainLR == 0);
	}
	
	/**
	 * Condition d'attente
	 * @return True : Le train peut entrer dans le sens LR
	 */
	public boolean getAccessLR() {
    	return (trainRL == 0);
	}
	
	//Méthode d'affichage des compteurs
	public int getRL() {
		return trainRL;
	}
	//Méthode d'affichage des compteurs
	public int getLR() {
		return trainLR;
	}

	/**
	 * Un train entre sur la voie ferrée 
	 * dans le sens gauche à droite.
	 * Section critique 
	 */
	public synchronized void addLR() {
		this.trainLR++;	
	}	
	
	/**
	 * Un train sort de la voie ferrée 
	 * dans le sens gauche à droite.
	 * Section critique 
	 */
	public synchronized void minLR() {
		if(this.trainLR > 0)
			this.trainLR--;	
	}
	
	/**
	 * Un train entre sur la voie ferrée 
	 * dans le sens droite-gauche.
	 * Section critique 
	 */
	public synchronized void addRL() {
		this.trainRL++;	
	}
	
	/**
	 * Un train sort de la voie ferrée 
	 * dans le sens droite-gauche.
	 * Section critique 
	 */
	public synchronized void minRL() {
		if(this.trainRL > 0)
			this.trainRL--;		
	}
	

}
