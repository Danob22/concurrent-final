package train;
/**
 * 
 * @author  Arias et Ducuara
 *
 */
public class NotPlaceGareException extends Exception{
	private static final long serialVersionUID = 1L;

	public NotPlaceGareException(String name){
		super(name);
	}
	public String getMessage(){
		return super.getMessage();
	}
}


