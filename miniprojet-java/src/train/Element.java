package train;



/**
 * Cette classe abstraite est la représentation générique d'un élément de base d'un
 * circuit, elle factorise les fonctionnalitÃ©s communes des deux sous-classes :
 * l'entrée d'un train, sa sortie et l'appartenance au circuit.<br/>
 * Les deux sous-classes sont :
 * <ol>
 *   <li>La représentation d'une gare : classe {@link Station}</li>
 *   <li>La représentation d'une section de voie ferrée : classe {@link Section}</li>
 * </ol>
 * 
 * @author Fabien Dagnat <fabien.dagnat@imt-atlantique.fr>
 * @author Philippe Tanguy <philippe.tanguy@imt-atlantique.fr>
 * Modifié par: Arias et Ducuara
 */
public abstract class Element {
	private final String name;
	protected Railway railway; //heritage
    

	protected Element(String name) {
		if(name == null)
			throw new NullPointerException();
		
		this.name = name;
	}

	public void setRailway(Railway r) {
		if(r == null)
			throw new NullPointerException();
		
		this.railway = r;
	}

	@Override
	public String toString() {
		return this.name;
	}
	
	//Chaque élément décide de son accès	
	public abstract boolean getAccess();
	//Chaque élément décide de la sortie		
	public abstract boolean getLeave(Direction d);
	
	//Chaque élément modifie ses variables dans l'entrée	
	public abstract void enter(Direction d);
	//Chaque élément modifie ses variables dans la sortie	
	public abstract void leave(Direction d);
	
}
