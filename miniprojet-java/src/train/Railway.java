package train;



/**
 * Représentation d'un circuit constitué d'éléments de voie ferrée : gare ou
 * section de voie
 * 
 * @author Fabien Dagnat <fabien.dagnat@imt-atlantique.fr>
 * @author Philippe Tanguy <philippe.tanguy@imt-atlantique.fr>
 * Modifié par  : Arias et Ducuara
 * La méthode qui gere est (enter)
 */
public class Railway {
	private final Element[] elements;
    protected Road road; // Contrôleur de voie
    
	public Railway(Element[] elements) {
		if(elements == null)
			throw new NullPointerException();
		
		this.elements = elements;
		for(Element e: this.elements)
			e.setRailway(this);
		
		this.road = new Road("Road"); // Contrôleur de voie
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		boolean first = true;
		for (Element e : this.elements) {
			if (first)
				first = false;
			else
				result.append("--");
			result.append(e);
		}
		return result.toString();
	}
	
	/**
	 * Méthode qui gère le mouvement d'un train lorsqu'il a fait un appel.
	 * Le train ne peut se déplacer que s'il a accès à l'élément suivant 
	 * et peut sortir de l'élément courant
	 * @param current position du train
	 * @return next position du train valide
	 */
	public synchronized Position enter(Position current) {
		Element curr = current.getElement();   	//:Obtenir l'element
		Position next = nextPosition(current);	//:Trouver le prochain element
		
		// Condition d'attente general
		while(!((next.getElement().getAccess())	&&(curr.getLeave(next.getDirection())))) {
			try {
				wait(); //attend
			}catch(InterruptedException e) {
				
			}
		}
		System.out.println("Go"); //:Il a reçu l'autorisation		
		curr.leave(next.getDirection());   //:Il fait l'sortié de l'anterieur element
		next.getElement().enter(next.getDirection()); //:Il fait l'entré au nouvel element
		notifyAll(); //Notify others
		return next;		
	}
	
	/**
	 * Méthode pour trouver l'index d'un Element t dans la liste d'elements
	 * @return index trouvée
	 */
	private int findIndex(Element t) {
	      int i = 0; 
	      while (i < this.elements.length) { 	  
	            if (this.elements[i] == t) { 
	                return i; 
	            } 
	            else { 
	                i = i + 1; 
	            } 
	      } 
	      return -1; //pas d'element (exception)
	}  
	
	/**
	 * Méthode pour trouver le prochain element
	 * @return next Position
	 */
	private Position nextPosition(Position currentPos) {
		Direction currentDirection = currentPos.getDirection();
		int index = findIndex(currentPos.getElement());
		Position nextPosition;			
		if(currentDirection == Direction.LR) {
			if(index == elements.length - 1) { //Est le dernier élément du chemin de fer ?  
				nextPosition = new Position(this.elements[index-1],  Direction.RL);				
			}else{
				nextPosition  = new Position(this.elements[index+1],  currentDirection);		
			}
		}else {
			if(index == 0) { //Est le premier élément du chemin de fer ?  		
				
				nextPosition  = new Position(this.elements[index+1],  Direction.LR);				
			}else {							
				nextPosition  = new Position(this.elements[index-1],  currentDirection);	
			}
		}
		return nextPosition;
	}
	
	
	
	
	
	
	
}
