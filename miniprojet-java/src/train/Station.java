package train;

/**
 * Représentation d'une gare. C'est une sous-classe de la classe {@link Element}.
 * Une gare est caractérisée par un nom et un nombre de quais (donc de trains
 * qu'elle est susceptible d'accueillir à un instant donné).
 * 
 * @author Fabien Dagnat <fabien.dagnat@imt-atlantique.fr>
 * @author Philippe Tanguy <philippe.tanguy@imt-atlantique.fr>
 * Modifié par  : Arias et Ducuara
 */
public class Station extends Element {
	
	private final int size;  //limit nombre de quais
	private int nbTrain;	 //Variable pour assurer l'invariant

	public Station(String name, int size) {
		super(name);
		if(name == null || size <=0)
			throw new NullPointerException();
		this.size = size;
		this.nbTrain = 0;
	}
	
	/**
	 * Condition d'attente
	 * @return True : Le train peut entrer
	 */
	@Override
	public  boolean getAccess() {
		return (this.nbTrain < this.size);
	}
	
	/**
	 * La condition de sortie d'une gare est :
	 *  Il y a des trains dans la gare et
     *  l'access au road est garantie dans la direction donnée
	 * @return True : Le train peut sortir de la gare
	 */
	@Override
	public boolean getLeave(Direction d) {	
		if(d == Direction.LR)
			return ((this.nbTrain > 0) && super.railway.road.getAccessLR());
		return ((this.nbTrain > 0) && super.railway.road.getAccessRL());					
	}
	
	/**
	 * Un train entre dans la gare, le [nbTrain] augmente
	 * et le nombre de trains sur la voie diminue.
	 * 
	 */
	@Override
	public synchronized void enter(Direction d) {
		this.nbTrain++;			
		this.minRoad(d); 
	}
	
	/**
	 * Un train sort de la gare, le [nbTrain] diminue
	 * et le nombre de trains sur la voie augmente.
	 */
	@Override
	public synchronized void leave(Direction d) {
		this.nbTrain--;	
		this.addRoad(d);
	}
	

	/**
	 * Augmente le nombre de trains sur la voie 
	 * dans la direction [d] donnée
	 */
	private void addRoad(Direction d) {
		if(d == Direction.LR)
			super.railway.road.addLR();
		else 
			super.railway.road.addRL();					
	}
	
	
	/**
	 * Disminue le nombre de trains sur la voie 
	 * dans la direction donnée
	 */
	private void minRoad(Direction d) {
		if(d == Direction.LR)
			super.railway.road.minLR();
		else 
			super.railway.road.minRL();					
	}

}
